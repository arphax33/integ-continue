package fr.epsi.continuousInteg.business.services;

import fr.epsi.continuousInteg.domain.entities.Article;
import fr.epsi.continuousInteg.domain.entities.Client;
import fr.epsi.continuousInteg.domain.entities.Contract;
import junit.framework.TestCase;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ArticleServiceTest extends TestCase {
    @Autowired
    ArticleService articleService;

    Article article;
    Contract contract;
    Client client;

    public void setUp() throws Exception {
        article = new Article();
        article.setPrice(50);

        contract = new Contract();
        contract.setId(1);
        contract.setName("Contract name");

        client = new Client();
        client.setId(1);
        client.setUsername("Client name");

        super.setUp();
    }

    @Test
    public void testApplyMargin10Percent() {
        contract.setMarge(10);
        client.setContract(contract);

        assertEquals(55, articleService.applyMargin(article, client));
    }
    @Test
    public void testApplyMargin5Percent() {
        contract.setMarge(5);
        client.setContract(contract);

        assertEquals(52.5, articleService.applyMargin(article, client));
    }

    @Test
    public void testApplyMargin25Percent() {
        contract.setMarge(25);
        client.setContract(contract);

        assertEquals(62.5, articleService.applyMargin(article, client));
    }

    @Test
    public void testApplyMargin2Percent() {

        try {
            contract.setMarge(2);
            client.setContract(contract);
            articleService.applyMargin(article, client);
            assertFalse("There is no exception",true);
        }
        catch (Exception e) {
            assertTrue("Exception has been catch",true);
        }
    }

    @Test
    public void testApplyMargin30Percent() {
        try {
            contract.setMarge(30);
            client.setContract(contract);
            articleService.applyMargin(article, client);
            assertFalse("There is no exception",true);
        }
        catch (Exception e) {
            assertTrue("Exception has been catch",true);
        }
    }
}