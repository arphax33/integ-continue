package fr.epsi.continuousInteg.domain.repositories;

import fr.epsi.continuousInteg.domain.entities.Contract;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContractRepository extends JpaRepository<Contract, Integer> {
}
