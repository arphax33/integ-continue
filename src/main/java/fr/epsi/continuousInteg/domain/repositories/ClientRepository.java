package fr.epsi.continuousInteg.domain.repositories;

import fr.epsi.continuousInteg.domain.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Integer> {
}
