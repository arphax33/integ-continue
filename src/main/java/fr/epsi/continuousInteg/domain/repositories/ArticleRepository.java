package fr.epsi.continuousInteg.domain.repositories;

import fr.epsi.continuousInteg.domain.entities.Article;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticleRepository extends JpaRepository<Article, Integer> {
}
