package fr.epsi.continuousInteg.domain.repositories;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.epsi.continuousInteg.domain.entities.Client;


public interface UserRepository extends JpaRepository<Client, Integer> {

    boolean existsByUsername(String username);

    Client findByUsername(String username);

    @Transactional
    void deleteByUsername(String username);

}