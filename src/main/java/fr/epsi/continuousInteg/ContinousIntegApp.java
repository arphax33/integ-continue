package fr.epsi.continuousInteg;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
public class ContinousIntegApp {
	public static void main(String[] args) {
		SpringApplication.run(ContinousIntegApp.class, args);
	}
}