package fr.epsi.continuousInteg.business.services;

import fr.epsi.continuousInteg.business.dtos.ArticleDTO;
import fr.epsi.continuousInteg.domain.entities.Article;
import fr.epsi.continuousInteg.domain.entities.Client;
import fr.epsi.continuousInteg.domain.repositories.ArticleRepository;

import java.util.ArrayList;
import java.util.List;

public class ArticleService {
    private final ArticleRepository repository;

    ArticleService(ArticleRepository repository) {
        this.repository = repository;
    }

    int applyMargin(Article article, Client client) {
        return article.getPrice() * client.getContract().getMarge() / 100;
    }
}
