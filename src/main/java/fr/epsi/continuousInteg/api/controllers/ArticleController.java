package fr.epsi.continuousInteg.api.controllers;

import fr.epsi.continuousInteg.business.dtos.ArticleDTO;
import fr.epsi.continuousInteg.domain.entities.Article;
import fr.epsi.continuousInteg.domain.repositories.ArticleRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ArticleController {


    private final ArticleRepository repository;

    ArticleController(ArticleRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/articles")
    public List<ArticleDTO> getArticles(String token) {

        List<ArticleDTO> articlesDTO = new ArrayList<ArticleDTO>();

        List<Article> articles = repository.findAll();

        articles.forEach(article -> articlesDTO.add(new ArticleDTO(article.getId(),article.getName(), article.getPrice())));

        return articlesDTO;
    }
}