/*########################### TABLE ###########################*/
CREATE TABLE article
(
    id   integer AUTO_INCREMENT NOT NULL,
    name varchar               NOT NULL
);

CREATE TABLE contract
(
    id   integer AUTO_INCREMENT NOT NULL,
    name varchar               NOT NULL,
    client_id   integer NOT NULL
);

CREATE TABLE client
(
    id        integer AUTO_INCREMENT NOT NULL,
    name      varchar               NOT NULL,
    pseudo    varchar               NOT NULL,
    password varchar               NOT NULL
);

/*########################### TABLE ASSOC ###########################*/
CREATE TABLE contract_article
(
    contract_id integer NOT NULL,
    article_id integer NOT NULL
);

/*########################### PRIMARY KEYS ###########################*/
ALTER TABLE article
    ADD constraint article_pk PRIMARY KEY CLUSTERED (id);

ALTER TABLE client
    ADD constraint client_pk PRIMARY KEY CLUSTERED (id);

ALTER TABLE contract
    ADD constraint contract_pk PRIMARY KEY CLUSTERED (id);

/*########################### FOREIGN KEYS ###########################*/
ALTER TABLE contract
    ADD CONSTRAINT contract_client_fk FOREIGN KEY (client_id) REFERENCES client (id) ON DELETE NO ACTION ON UPDATE no action;

ALTER TABLE contract_article
    ADD
        CONSTRAINT contract_article_contract_fk FOREIGN KEY (contract_id) REFERENCES contract (id) ON DELETE NO ACTION ON UPDATE no action;

ALTER TABLE contract_article
    ADD
        CONSTRAINT contract_article_article_fk FOREIGN KEY (article_id) REFERENCES article (id) ON DELETE NO ACTION ON UPDATE no action;