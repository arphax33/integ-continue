/*########################### CLIENT ###########################*/
INSERT INTO client
(name, pseudo, password)
VALUES
('CLIENT_1', 'CLIENT_1', 'CLIENT_1');

/*########################### ARTICLE ###########################*/
INSERT INTO article
(name)
VALUES
('ARTICLE_1');

/*########################### CONTRACT ###########################*/
INSERT INTO contract
(name, client_id)
VALUES
('CONTRACT_1', 1);

/*########################### CONTRACT_ARTICLE ###########################*/
INSERT INTO contract_article
(contract_id, article_id)
VALUES
(1, 1);